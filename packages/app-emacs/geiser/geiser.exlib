# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require elisp-module

export_exlib_phases src_install

SUMMARY="GNU Emacs and Scheme talk to each other"
DESCRIPTION="
Geiser is a generic Emacs/Scheme interaction mode, featuring an enhanced REPL and a set of minor
modes improving Emacs' basic scheme major mode. The main functionalities provided are:

  - Evaluation of forms in the namespace of the current module.
  - Macro expansion.
  - File/module loading.
  - Namespace-aware identifier completion (including local bindings, names visible in the current
    module, and module names).
  - Autodoc: the echo area shows information about the signature of the procedure/macro around point
    automatically.
  - Jump to definition of identifier at point.
  - Access to documentation (including docstrings when the implementation provides it).
  - Listings of identifiers exported by a given module.
  - Listings of callers/callees of procedures.
  - Rudimentary support for debugging (list of evaluation/compilation error in an Emacs'
    compilation-mode buffer).
"
HOMEPAGE="http://www.nongnu.org/${PN}/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        app-editors/emacs
    recommendation:
        dev-lang/guile:2.0
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --with-lispdir=${ELISP_SITE_LISP}/${PN} )

geiser_src_install() {
    default;
    elisp-install-site-file
}

